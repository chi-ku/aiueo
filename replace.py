#!/bin/env python

import re


def main():
    lines = []
    with open("index.html") as f:
        lines = f.readlines()
        pass
    print(lines)

    pat = re.compile("\<div\>(?P<letter>.*)\</div\>")

    def rep(line):
        m = pat.search(line)
        if m:
            return "<div class='letter'><a class='btn' href='#'>{}</a></div>".format(m.groups()[0])
        return line

    lines2 = map(rep, lines)
    print(list(lines2))
    with open("test2.html", 'w') as f:
        f.writelines(list(lines2))
    pass


if __name__ == "__main__":
    main()
